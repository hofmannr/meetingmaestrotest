'use strict';
module.change_code = 1;
var _ = require('lodash');
var MEETING_DATA_TABLE_NAME = 'MeetingData';

//var dynasty = require('dynasty')({});
//var credentials = {
//  accessKeyId: '<YOUR_ACCESS_KEY'>,
//  secretAccessKey: '<YOUR_SECRET_ACCESS_KEY'>
//};
var localUrl = 'http://localhost:4000';
var localCredentials = {
    region: 'us-east-1',  //default
    accessKeyId: 'fake',
    secretAccessKey: 'fake'
};

var localDynasty = require('dynasty')(localCredentials, localUrl);
var dynasty = localDynasty;

function DatabaseHelper() {}
var meetingTable = function() {
    return dynasty.table(MEETING_DATA_TABLE_NAME);
};

DatabaseHelper.prototype.createMeetingTable = function() {
    console.log('\nCreating DB Table: ' + MEETING_DATA_TABLE_NAME);
    return dynasty.describe(MEETING_DATA_TABLE_NAME)
        .catch(function(error) {
            return dynasty.create(MEETING_DATA_TABLE_NAME, {
                key_schema: {
                    hash: ['meetingId', 'string']
                }
            });
        });
};

DatabaseHelper.prototype.storeMeetingData = function(userId, meetingData) {
    console.log('\nSaving Meeting Data: ');
    console.log(meetingData);
    return meetingTable().insert({
        meetingId: userId,
        data: meetingData
    }).catch(function(error) {
        console.log(error);
    });
};

DatabaseHelper.prototype.readMeetingData = function(userId) {
    console.log('\nReading from DB.');
    return meetingTable().find(userId)
        .then(function(result) {
            return result;
        }).catch(function(error) {
            console.log(error);
        });
};

module.exports = DatabaseHelper;
