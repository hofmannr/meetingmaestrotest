'use strict';
module.change_code = 1;
var _ = require('lodash');
var Skill = require('alexa-app');
var MEETING_SESSION_KEY = 'meeting';
var skillService = new Skill.app('meetingmaestro');
var MeetingHelper = require('./meeting_helper');
var DatabaseHelper = require('./database_helper');
var databaseHelper = new DatabaseHelper();


///////////////////////////////////////////////////////////////////////////////
//////////////          EVENTS
///////////////////////////////////////////////////////////////////////////////

// will execute before any intent is handled
// if table already created will return an error
// error is handled in dbhelper class
skillService.pre = function(request, response, type) {
    databaseHelper.createMeetingTable();
}

skillService.launch(function(request, response) {
    var prompt = 'Welcome to Meeting Maestro! To start a meeting, say begin meeting.';
    response.say(prompt)
            .shouldEndSession(false);
})

///////////////////////////////////////////////////////////////////////////////
//////////////          MEETING INTENTS
///////////////////////////////////////////////////////////////////////////////
skillService.intent('NewMeetingIntent', {
    'utterances': ['{new|start|begin} {|taking} {|meeting}']
}, function(request, response) {
    var userId = request.userId;
    // TODO Make meetingId concat of userId and date
    var meetingHelper = new MeetingHelper();
    meetingHelper.meetingId = userId;
    meetingHelper.data.userId = userId;
    saveMeeting(meetingHelper, request);
    response.say('Starting meeting')
            .shouldEndSession(false);
});
skillService.intent('TellMeetingIntent', {
    'utterances': ['{tell me|repeat|list} meeting']
}, function(request, response) {
    //TODO
});
skillService.intent('ResetMeetingIntent', {
    'utterances': ['{reset|clear} meeting']
}, function(request, response) {
    //TODO
});
skillService.intent('LoadMeetingIntent', {
    'utterances': ['{load|resume} {|a|the} {|last|current|previous} meeting']
}, function(request, response) {
    var userId = request.userId;
    databaseHelper.readMeetingData(userId).then(function(result){
        return (result === undefined ? {} : JSON.parse(result['data']));
    }).then(function(loadedMeetingData) {
        var meetingHelper = new MeetingHelper(loadedMeetingData);
        return meetingIntentFunction(meetingHelper, request, response);
    });
    return;
});
skillService.intent('EndMeetingIntent', {
    'utterances': ['{end|stop|terminate|conclude} {|this} meeting']
}, function(request, response) {
    var meetingHelper = getMeetingHelperFromRequest(request);
    meetingHelper.data.completed = true;
    saveMeeting(meetingHelper, request);
    response.say('Meeting has ended, everyone disperse!')
            .shouldEndSession(true)
            .send();
    return;
});
///////////////////////////////////////////////////////////////////////////////
//////////////          ATTENDANCE INTENTS
///////////////////////////////////////////////////////////////////////////////
var attendanceSlots = {
    'UserName': 'AMAZON.US_FIRST_NAME',
    'SecondUserName': 'AMAZON.US_FIRST_NAME',
    'LastName': 'LIST_OF_LAST_NAMES', // Are there no AMAZON.US_LAST_NAME?
    'SecondLastName': 'LIST_OF_LAST_NAMES'
};
skillService.intent('AddUserIntent', {
    'slots': attendanceSlots,
    'utterances': ['{|add|insert|apply} {-|UserName} {-|LastName} {|and} {-|SecondUserName} {-|SecondLastName}']
}, function(request, response) {
    // TODO Setup textHelper.js - but probably don't do that
    var newAttendantName = request.slot("UserName");
    if (!newAttendantName) {
        response.say('Ok, who do you want to add?')
                .reprompt('Who do you want to add?');
        return;
    }
    var newLastName = request.slot("LastName");
    if (newLastName) {
        newAttendantName = newAttendantName + " " + newLastName;
    }
    var secondAttendantName = request.slot("SecondUserName");
    var secondLastName = request.slot("SecondLastName");
    //What if only second first name?
    if (secondAttendantName && secondLastName) {
        secondAttendantName = secondAttendantName + " " + secondLastName;
    }
    var meetingHelper = getMeetingHelperFromRequest(request);
    meetingHelper.data.attendees.push(newAttendantName);
    if (secondAttendantName) meetingHelper.data.attendees.push(secondAttendantName);
    saveMeeting(meetingHelper, request)
    response.say('Attendees have been saved!')
            .shouldEndSession(false)
            .send();
    return;
});
skillService.intent('MissingUserIntent', {
    'slots': attendanceSlots,
    'utterances': ['{|missing|update} {-|UserName} {-|LastName} {|and} {-|SecondUserName} {-|SecondLastName} {|missing}']
}, function(request, response) {
    // TODO Setup textHelper.js... or you know, like don't...
    var newMissing = request.slot("UserName");
    if (!newMissing) {
        response.say('Ok. Who do you want to add or is missing?')
                .reprompt('Who do you want to add?');
        return;
    }
    var newMissingLastName = request.slot("LastName");
    if (newMissingLastName) {
        newMissing = newMissing + " " + newMissingLastName
    }
    var secondMissing = request.slot("SecondUserName");
    var secondMissingLastName = request.slot("SecondLastName");
    // What if only missing first name present?
    if (secondMissing && secondMissingLastName) {
        secondMissing = secondMissing + " " + secondMissingLastName;
    }
    var meetingHelper = getMeetingHelperFromRequest(request);
    meetingHelper.data.missing.push(newMissing);
    if (secondMissing) meetingHelper.data.missing.push(secondMissing);
    console.log('\nMeetingHelper: ');
    console.log(meetingHelper);
    saveMeeting(meetingHelper, request)
    response.say('Missing attendees have been saved!')
            .shouldEndSession(false)
            .send();
    return;
});

///////////////////////////////////////////////////////////////////////////////
//////////////          DECISION ITEMS INTENTS
///////////////////////////////////////////////////////////////////////////////
var decsionsDictionary = { wordCounts: [
    'one two three four five',
    'one two three four five six',
    'one two three four five six seven',
    'one two three four five six seven eight',
    'one two three four five six seven eight nine',
    'one two three four five six seven eight nine ten']
};
skillService.intent('AddDecisionPointIntent', {
    'slots': { 'decision': 'AMAZON.LITERAL' },
    'utterances': ['{add|update|create} {|new} decision {point|item} {|to} {wordCounts|decision}']
}, function(request, response) {
    console.log('\nAdding Decision. /  Request: ');
    console.log(request);
    var newDecisionPoint = request.slot("decision");
    if (!newDecisionPoint) {
        response.say('Ok. What has been decided?')
                .reprompt('What do you want to add?');
        return;
    }
    var meetingHelper = getMeetingHelperFromRequest(request);
    meetingHelper.data.decisions.push(newDecisionPoint);
    saveMeeting(meetingHelper, request)
    response.say('Decision has been saved!')
            .shouldEndSession(false)
            .send();
    return;
});
skillService.intent('TellDecisionPointIntent', {
    'utterances': ['{tell|repeat|what} decisions {|were made} for this meeting']
}, function(request, response) {
    var meetingHelper = getMeetingHelperFromRequest(request);
    if (meetingHelper.data.decisions.length === 0) {
        response.say('No decisions points in this meeting.')
                .shouldEndSession(false)
                .send();
        return;
    }
    var speechOutput = 'The following items have been decided on: ';
    var len = meetingHelper.data.decisions.length;
    meetingHelper.data.decisions.forEach(function(index, decision) {
        speechOutput += 'Number ' + (index+1) + decision;
        if (index < len) {
            speechOutput += ' and '
        }
    });
    response.say(speechOutput)
            .shouldEndSession(false)
            .send();
    return;
});
skillService.intent('RemoveMeetingPointIntent', {
    'slots': { 'index': 'AMAZON.NUMBER' },
    'utterances': ['{remove|delete|wipe from existence} decision {point|item} number {-|index}']
}, function(request, response) {
    var meetingHelper = getMeetingHelperFromRequest(request);
    if (meetingHelper.data.decisions.length === 0) {
        response.say('No decisions points to delete.')
                .shouldEndSession(false)
                .send();
        return;
    }
    var index = request.slots("index");
    if ((index === undefined) || (index <= 0)) {
        response.reprompt('Which decision would you like to delete.');
        return;
    }
    meetingHelper.data.decisions.splice(index-1, 1);
    saveMeeting(meetingHelper, request);
    response.say('Decision number '+index+' has been deleted.')
            .shouldEndSession(false)
            .send();
})
///////////////////////////////////////////////////////////////////////////////
//////////////          HELPER FUNCTIONS
///////////////////////////////////////////////////////////////////////////////

var getMeetingHelper = function(meetingHelperData) {
    if (meetingHelperData === undefined) {
        console.log('\n MeetingHelperData was undefined Yo')
        meetingHelperData = {};
    }
    return new MeetingHelper(meetingHelperData);
}

var getMeetingHelperFromRequest = function(request) {
    var session = request.getSession();
    var meetingHelperData = session.get(MEETING_SESSION_KEY);
    return getMeetingHelper(meetingHelperData);
}

var meetingIntentFunction = function(meetingHelper, request, response) {
    console.log(meetingHelper);
    if (meetingHelper.isCompleted()) {
        response.say('This concludes today\'s meeting!')
                .shouldEndSession(true);
    } else {
        response.say(meetingHelper.getPrompt())
                .reprompt('I didn\'t hear you. ' + meetingHelper.getPrompt())
                .shouldEndSession(false)
                .send();
    }
    response.getSession().set(MEETING_SESSION_KEY, meetingHelper);
    response.send();
}

// The intention is to call this method during any sort of update commands
// When resuming session will find by meeting id and !isCompleted to know which meeting to resume
var saveMeeting = function(meetingHelper, request) {
    //TODO meetingID is concat of userId and date
    var userId = request.userId;
    request.getSession().set(MEETING_SESSION_KEY, meetingHelper);
    databaseHelper.storeMeetingData(userId, JSON.stringify(meetingHelper))
        .then(function(result) {
            console.log('\nResults: ');
            console.log(result);
            return result;
        }).catch(function(error) {
            console.log('\nError: ');
            console.log(error);
        })
}

module.exports = skillService;
