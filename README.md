# README #

This repo is meant to be a quick demo for using the [alexa-app](https://www.npmjs.com/package/alexa-app) module as a potential solution. I will try to quickly show how to use it and its benefits.

Incorporating this module will speed up dev time by streamlining some more tedious aspect of Alexa skill development, provide a way to manage sessions, simplified request handling and response generation.

I use a few other tools and libraries to make my life easier, and do things like test and debug code locally. I will document those below as well for any one interested.

Finally, I set up a local instance of [DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html) to play with. 

# Project Overview #

This project has 3 main files:

* index.js
+ meeting_helper.js
- database_helper.js

index.js is where we define our app, intents, and logic.

meeting_helper.js contains the meeting data structure and information about our meeting from the current session. We also define additional methods here to help grab and format the data that we need.

database_helper.js is similar to storage.js in the current project. I am using dynasty to interact with a local dynamo instance. We would just switch the local credentials for production. 

# Benefits #


### Writing Intents ###

Once we've defined and initialized our Alexa app:

```
#!javascript

var Skill = require('alexa-app');
var SkillService = new Skill.app('meetingmaestro');
```

We can add our intents, as such:


```
#!javascript

app.intent('NewMeetingIntent', {
    'slots':      { /* slot definitions */ },
    'utterances': [ /* utterance expression */]
}, function(request, response) {
    // intent logic here
});
```

Events can also be defined in a similar manner

```
#!javascript

app.launch(function(request, response) {/*...*/});
app.sessionEnded(function(request, response) {/*...*/});
```

### Auto-Generating Intent Schemas and Sample Utterances ###

When defining our intents we can define slots by passing it a slots obj

```
#!javascript
"slots": {
  "NAME": "AMAZON.US_FIRST_NAME",
  "AGE": "AMAZON.NUMBER"
}
```

And a pattern for generating sample utterances

```
#!javascript
"utterances": [
  "my {name is|name's} {NAME} and {I am|I'm} {-|AGE}{ years old|}"
]
```

When you run this project, the intents you define will expand out to auto-generate your intent schema and sample utterances to be used in the skill config. 

### Persisting Sessions ###

This module provides a number of really useful features for interacting with the response, request, and session objects used by Alexa. Most notably we can persist sessions across multiple request very easily. Before returning from an intent handler, we just let the response know if it should keep the session open or close it. 

`response.shouldEndSession(false);`

### Saving Sessions ###

In this project, we use meeting_helper.js as our Meeting class. This will house any data about the meeting, its data structure, and any helper methods you wish to add. The meetingHelper obj will get associated with the current session.

We write a helper method saveMeeting() in index.js. All methods that update information held in meetingHelper will call this function and pass their copy of meetingHelper to it. It doesn't matter if we are adding, updating, or deleting. This method will call our databaseHelper to make the appropriate call. 

We modify our local copy of meetingHelper and then save it back to the DB. 

### Loading Data ###



# Installation #

*For this demo, I used Node v0.10.36*

### Getting the Project ###

1. Clone copy of project to a local directory:
`git clone https://bitbucket.org/hofmannr/meetingmaestrotest.git`

2. `cd` into the meetingmaestro directory and run `npm install`

### DynamoDB ###

1. Download [this](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html) to get a local instance of DynamoDB

2. Extract the contents of the file you downloaded to a directory called db. You will need java to run the local instance.

3. Run `./launchDB.sh` from the project root directory (this is just a bash script I wrote, b/c I got really tired of typing the same commands over and over again.) 

4. You can navigate to localhost:4000/shell (this should be the default location, you can modify the script and pass it flags for were to host your local instance).


# Testing Locally #

### alexa-app-server ###

This is a project I use to test the code I am working on locally.

1. Clone the alexa-app-server project into the root of this project:
`git clone https://github.com/matt-kruse/alexa-app-server.git`

2. Once downloaded, grab any additional dependencies with `npm install`

3. Running `node server` will serve up any projects that are in the alexa-app-server/examples/apps folder. Run `./launchServer.sh` to copy the contents of meetingmaestrotest to this folder. (Again just a script for commands I got tired of typing out).

This will serve any projects to http://localhost:8080. You will be able to select which intents to fire off, view the Request, Response, and Session JSONs, and view the sample utterances and intent schemas generated by alexa-app. Since this is run locally, you can also view console logs which is a godsend for debugging.


Now that you have hopefully gotten everything set up and running, I would like to highlight some of the main ways that this helps to make writing apps for Alexa simpler. 

# Going Further #

I wanted to bring the sample project here a bit further to be close to what our current dev branch is capable of. I think if you look at Decision Items and how I was beginning to lay out the rest you should get a strong idea of how you would proceed with your intents. We could make this more modular by bringing similar intents out into their own files. 

Also I have both mocha and chai included in the package.json as well as a test/ dir. You could write test for better quality software using these frameworks here.  

# Resources Used #

1. [alexa-app](https://github.com/alexa-js/alexa-app)

2. [dynasty](http://dynastyjs.com/) 

3. [DynamoDB](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html)

4. [alexa-utternaces](https://github.com/alexa-js/alexa-utterances)

5. [mocha]()

6. [chai]()

### Useful Tools ###

1. [ nvm ](https://github.com/creationix/nvm) - For MacOS only, but provides easy tool for maintaining many different NodeJs installations. Windows alternatives do exist in the wild.

2. [ AWS CLI ](https://aws.amazon.com/cli/) - Makes easy to read, and query what is in you DynamoDB instance from the command line.

3. [alexa-app-server](https://github.com/alexa-js/alexa-app-server)

### Additional Docs ###