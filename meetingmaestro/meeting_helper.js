'use strict';
module.change_code = 1;

function MeetingHelper(obj) {
    this.meetingId = '';
    this.data = {
        userId: '',
        attendees: [],
        missing: [],
        actionItems: [],
        decisions: [],
        completed: false
    };
    for (var prop in obj) this[prop] = obj[prop];
}

MeetingHelper.prototype.isCompleted = function() {
    //return whether or not the meeting is over
    //perhaps if time allotted for the meeting has expired
    //when ending meeting should manually set to true
    return this.completed;
};

MeetingHelper.prototype.getPrompt = function() {
    var help = 'You can add attendance, decision points, and action items, get meeting information, or say help. What would you like?';
    return help;
};

module.exports = MeetingHelper;
